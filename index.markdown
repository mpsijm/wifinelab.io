---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: page 
---
# Wi-Fine

**It is fine to use public Wi-Fi, even without a VPN.** Enjoy travelling and avoid high roaming costs! 

Nothing is 100% safe, but public Wi-Fi is such a small risk that it does not warrant the attention it still gets. Avoiding it has become a form of [security theater](https://en.wikipedia.org/wiki/Security_theater). Modern internet protocols are designed with the assumption of an untrusted network layer. If all networks are assumed to be untrusted, your choice of network does not matter. You should rely on the security that is built on top of the network layer, such as TLS.

Using public Wi-Fi was pretty dangerous when software like the Firefox plugin [Firesheep](https://en.wikipedia.org/wiki/Firesheep) were around in 2010, which could hijack a Facebook account with the click of a button. These times are over. When times change, the wisdom from that past era tends to stay around for a longer while. 

Some improvements that occurred over the last 10+ years:
* Almost the entire web moved to HTTPS. Exceptions are a rare sight these days. Additional, HSTS (with preloading) is quite widely deployed, especially at big cloud services. HSTS which makes plain text downgrade attacks hard to deploy. Due to the PCI-DSS, any retailer doing credit card transaction processing is forced to use TLS (There are certainly huge risks involved with credit cards, but those have nothing to do with public Wi-Fi). App Transport Security on iOS platforms enforces HTTPS from apps (see [here](https://developer.apple.com/documentation/bundleresources/information_property_list/nsapptransportsecurity)).
* Browsers are auto-updating, which is a very effective method of protecting against known vulnerabilities.
* Devices are better hardened and it is less common to have any daemons running that could pose a risk. Windows Vista (released in 2007) already made a pop-up window that disabled file sharing on public networks. In the early days of Android, its debugging feature was unauthenticated, but this is also no longer the case.

There seems to be notion that public Wi-Fi is insecure and LTE is secure. They might have some more regulations as they are generally run by big corporations, but systems like the [Diameter](https://en.wikipedia.org/wiki/Diameter_(protocol)) protocol are far from secure, as demonstrated in [this paper](https://www.riverpublishers.com/journal_read_html_article.php?j=JICTS/5/1/3). This is not problematic if you are not putting any trust into the network layer.

"Fixing" public Wi-Fi by using a VPN is not a good idea, as written in [this essay](https://gist.github.com/joepie91/5a9909939e6ce7d09e29) by joepie91. By using a VPN, you are only changing *who* can see your network layer traffic. It does not increase any security. It mentions "known hostile networks", but surely not all public Wi-Fi networks are known to be hostile. Usually they are providing an extra service to a customer of an enterprise, trying to inject malware might go against their business interest.

If you are a high profile target and you are afraid of someone hacking your smartphone through a zero-day in your Wi-Fi chipset firmware or a WebKit/Blink zero-day using the sign-up page: alright, turn it off, but you should probably do that with the rest of your network as well in that case. People have been infected with [Pegasus using an SMS](https://citizenlab.ca/2022/04/peace-through-pegasus-jordanian-human-rights-defenders-and-journalists-hacked-with-pegasus-spyware/) and [Bin Laden did not had internet access](https://www.computerworld.com/article/2508379/bin-laden-s-villa-lacked-internet--phone-service.html) to his hiding place but got caught anyway.

# My ISP/security vendor/bank says otherwise!
Yes, they commonly claim the opposite case here. I'm just a random guy with an opinion against large ISPs, banks and big security vendors. I ask you to look at the actual arguments they make and see who makes the best conclusion! I do commonly see the conclusion repeated without good reasoning why you should do it.

# Avoiding public Wi-Fi is defense in depth!
"Don't let the perfect be the enemy of the good", they say. I agree! You should not avoid a good security measure if there is an even better one that you can't take. This requires that the measure is in fact, *good*. The bare minimum is that the benefit of a measure have to outweigh the (potential) cost. Avoiding public Wi-Fi can be a very expensive measure if you have to pay large sums of money for data roaming or taking a more expensive cellular subscription.

Giving non-expert people the advice to avoid it is not a good idea. People have a limited memory for remembering security advices, so you better make sure that they use that limited memory for the most effective measures.

There is nothing wrong with avoiding Wi-Fi if you already have the luxury of unlimited cellular data. Disabling Wi-Fi will even improve your battery life. But don't avoid it for security reasons.

# There are protocols that send plain text over the network!
Classic DNS connections are sent over the network in plain text. But does this matter? Users mostly use DNS to look up an A record for a web host, which uses TLS. An attacker could do something with cache poisoning, but with TLS, this is irrelevant because the (server) endpoint still needs to prove that they own the private ky that corresponds to the certificate. Fixing DNS problems with DNSSEC is not effective either, as written [here](https://sockpuppet.org/blog/2015/01/15/against-dnssec/) by Thomas H. Ptacek.

Enterprise devices can have protocols enabled that can potentially be exploited, such as mDNS or LLMNR. Some relay attacks with DHCPv6 might also be possible. Note that this is mostly exclusive to Windows devices and does not apply to Apple and Android smartphones. Still, this does require some level of sophistication from the attacker side and you have to decide how much of this should be included in your threat model. I would still argue that for the average user, this does not matter.

# Then what should I do?
* Update your software and try to buy hardware from vendors that are known to deliver updates for a longer period.
* Use an ad blocker, uBlock Origin is fine.
* Use a password manager [that is built-in in your browser](https://lock.cmpxchg8b.com/passmgrs.html). Others might be fine as well, but here you can benefit from the security engineering done by the team that made the browser itself. No IPC that goes outside the browser is required this way.
* Use multi-factor authentication, preferably with FIDO2.

## Own infrastructure
Running your own public access point can however be tricky in a legal sense. It differs between jurisdictions, but you might be liable for the traffic that originates from your IP adress(es), which could cause problems with your local law enforcement agency. Use a strong WPA passphrase on your home Wi-Fi and use the guest feature with temporary accounts if that is available. Try to avoid using the router provided by your ISP, who generally have terrible security. Use a supported and updated build of OpenWrt.

If you are running a website: deploy HTTPS with HSTS (be careful with this, especially preloading), this protects users on untrusted networks, including public Wi-Fi.

# About
Written by [Eloy Degen](https://eloydegen.com) and published using a public Wi-Fi network. Released under the [CC0 license](https://creativecommons.org/share-your-work/public-domain/cc0/) in 2022. See something crucial that is missing? Merge requests welcome at [the GitLab repo](https://gitlab.com/wifine/wifine.gitlab.io).

Comment on [Lobste.rs](https://lobste.rs/s/ljdnnd/wi_fine_it_is_fine_use_public_wi_fi) or [Hacker News](https://news.ycombinator.com/item?id=32315579).